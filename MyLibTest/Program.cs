﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Rafka.MathLib.Utils;
using Rafka.MathLib.Real;
using Rafka.MathLib.Real.Numerics;

namespace MyLibTest {
    class Program : ODE {
        private const double gL = 0.003;        // mS/mm^2
        private const double gK = 0.36;         // mS/mm^2
        private const double gNa = 1.2;         // mS/mm^2
        private const double EL = -54.387;      // mV
        private const double EK = -77;          // mV
        private const double ENa = 50;          // mV
        private const double cm = 1e-5;         // mF/mm^2
        //private const double cm = 10;         // nF/mm^2
        //private const double Ie = 5;          // nA
        private const double Ie = 0.1;   	    // uA
        private const double T = 0.1;           // s
        private const double timeScale = 1000;  // 1000:s 1:ms  

        public Program() {
            /*
             * x0[0] = V
             * x0[1] = n
             * x0[2] = m
             * x0[3] = h
             */
            ts = 0.0;
            te = 1.0;
            double V0 = -65;
            //double i = im(V0, n_inf(V0), m_inf(V0), h_inf(V0));
            x0 = new Vector(V0, n_inf(V0), m_inf(V0), h_inf(V0));
        }

        static void Main(string[] args) {
            StreamWriter sw = new StreamWriter(@"data");
            Gnuplot gp = new Gnuplot();

            gp.Start();
            gp.SetXRange(0, 1);
            gp.SetYRange(-80, 60);
            //gnuplot.StandardInput.WriteLine("set zrange [-1.2:1.2]");

            ODESolver.dt = 1e-6;
            List<Vector> list = ODESolver.rk4(new Program());
			for (int i = 0; i < list.Count; i++) {
                sw.Write (list [i] [0]);
                for (int j = 1; j < list[i].Length; j++)
                  sw.Write(" " + list[i][j]);
                sw.WriteLine();
            }
            sw.Close();

            gp.StandardInput.WriteLine("plot \"data\" u 1:2 w l");

            Console.WriteLine("Programm finished.");
        }

        //正弦波の電極電流
        private double EC(double t) {
            return Ie * (Math.Sin(2.0 * Math.PI * t / T) + 1.0);
        }

        //n
        private double alpha_n(double V) {
            return (0.01 * (V + 55)) / (1.0 - Math.Exp(-0.1 * (V + 55))) * timeScale;
        }
        private double beta_n(double V) {
            return 0.125 * Math.Exp(-0.0125 * (V + 65)) * timeScale;
        }
        private double n_inf(double V) {
            return alpha_n(V) / (alpha_n(V) + beta_n(V));
        }

        //m
        private double alpha_m(double V) {
            return (0.1 * (V + 40)) / (1.0 - Math.Exp(-0.1 * (V + 40))) * timeScale;
        }
        private double beta_m(double V) {
            return 4.0 * Math.Exp(-0.0556 * (V + 65)) * timeScale;
        }
        private double m_inf(double V) {
            return alpha_m(V) / (alpha_m(V) + beta_m(V));
        }

        //h
        private double alpha_h(double V) {
            return 0.07 * Math.Exp(-0.05 * (V + 65)) * timeScale;
        }
        private double beta_h(double V) {
            return 1.0 / (1.0 + Math.Exp(-0.1 * (V + 35))) * timeScale;
        }
        private double h_inf(double V) {
            return alpha_h(V) / (alpha_h(V) + beta_h(V));
        }

        //膜電流
        private double im(double V, double n, double m, double h) {
            return gL * (V - EL) + gK * n * n * n * n * (V - EK) + gNa * m * m * m * h * (V - ENa);
        }

        //H-H方程式
        public override Vector Feval(double t, Vector x) {
            double V, n, m, h;
            Vector res = new Vector(x.Length);
            /*
             * res[0] = V
             * res[1] = n
             * res[2] = m
             * res[3] = h
             */
            V = x[0];
            n = x[1];
            m = x[2];
            h = x[3];

			res [0] = (-im (V, n, m, h) + ((0.5 < t && t < 0.75) ? Ie : 0)) / cm;
			res [1] = alpha_n (V) * (1 - n) - beta_n (V) * n;
			res [2] = alpha_m (V) * (1 - m) - beta_m (V) * m;
			res [3] = alpha_h (V) * (1 - h) - beta_h (V) * h;
            //res[4] = im(V, n, m, h);

            return res;
        }
    }
}
