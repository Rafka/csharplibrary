﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rafka.MathLib.Real;
using Rafka.MathLib.Real.Numerics;

namespace MyLibTest {
    class PlanetSim : ODE {
        const double AU = 149597870700.0;
        const double G = 6.67408 * 1e-11 / (AU * AU * AU) * (365.0 * 24 * 60 * 60) * (365 * 24 * 60 * 60);
        const double mS = 1.989 * 1e30;
        const double mE = 5.976 * 1e24;
        const double mM = 7.349 * 1e22;
        const double GS = G * mS;
        const double GE = G * mE;
        const double GM = G * mM;

        Vector rS, rE, rM, vS, vE, vM;

        public PlanetSim() {
            ts = 0.0;
            te = 3.0;
            ODESolver.dt = 1e-4;

            double rEM = 385000.0 / (AU / 1000.0);
            double v0M = 1.022 * 60 * 60 * 24 * 365 / (AU / 1000.0);

            rS = new Vector(0.0, 0.0, 0.0);
            rE = new Vector(1.0, 0.0, 0.0);
            rM = new Vector(1.0 + rEM, 0.0, 0.0);
            vS = new Vector(0.0, 0.0, 0.0);
            vE = new Vector(0.0, 2 * Math.PI, 0.0);
            vM = new Vector(0.0, 2*Math.PI + v0M, 0.0);
            x0 = new Vector(rS[0], rS[1], rS[2], rE[0], rE[1], rE[2], rM[0], rM[1], rM[2], vS[0], vS[1], vS[2], vE[0], vE[1], vE[2], vM[0], vM[1], vM[2]);
        }
        public override Vector Feval(double t, Vector x) {
            rS[0] = x[0]; rS[1] = x[1]; rS[2] = x[2];
            rE[0] = x[3]; rE[1] = x[4]; rE[2] = x[5];
            rM[0] = x[6]; rM[1] = x[7]; rM[2] = x[8];
            vS[0] = x[9]; vS[1] = x[10]; vS[2] = x[11];
            vE[0] = x[12]; vE[1] = x[13]; vE[2] = x[14];
            vM[0] = x[15]; vM[1] = x[16]; vM[2] = x[17];

            Vector rSM, rSE, rES, rEM, rMS, rME;
            rSM = rM - rS;
            rSE = rE - rS;
            rES = -rSE;
            rEM = rM - rE;
            rMS = -rSM;
            rME = -rEM;

            Vector fSM, fSE, fES, fEM, fMS, fME;
            fSM = (GM / (rSM.Size * rSM.Size * rSM.Size)) * rSM;
            fSE = (GE / (rSE.Size * rSE.Size * rSE.Size)) * rSE;
            fES = (GS / (rES.Size * rES.Size * rES.Size)) * rES;
            fEM = (GM / (rEM.Size * rEM.Size * rEM.Size)) * rEM;
            fMS = (GS / (rMS.Size * rMS.Size * rMS.Size)) * rMS;
            fME = (GE / (rME.Size * rME.Size * rME.Size)) * rME;

            Vector fS, fE, fM;
            fS = fSE + fSM;
            fE = fES + fEM;
            fM = fMS + fME;

            return new Vector(vS[0], vS[1], vS[2], vE[0], vE[1], vE[2], vM[0], vM[1], vM[2], fS[0], fS[1], fS[2], fE[0], fE[1], fE[2], fM[0], fM[1], fM[2]);
        }
    }
}
