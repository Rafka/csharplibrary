﻿using System;

namespace Rafka.TestLib {
    public class VectorN {
        private double[] v;

        public VectorN(int n) {
            v = new double[n];
            for (int i = 0; i < n; i++)
                v[i] = 0.0;
        }

        public VectorN(params double[] ps) {
            v = new double[ps.Length];
            for (int i = 0; i < ps.Length; i++)
                v[i] = ps[i];
        }

        public int Length {
            get { return v.Length; }
        }

        public double Size {
            get {
                double res = 0.0;
                for (int i = 0; i < v.Length; i++)
                    res += v[i] * v[i];
                return Math.Sqrt(res);
            }
        }

        public double this[int i] {
            get {
                if (i < v.Length)
                    return v[i];
                else
                    throw new ArgumentException("領域外を参照しています。");
            }
            set {
                if (i < v.Length)
                    v[i] = value;
                else
                    throw new ArgumentException("領域外を参照しています。");
            }
        }

        public static VectorN operator -(VectorN v) {
            VectorN res = new VectorN(v.Length);
            for (int i = 0; i < res.Length; i++)
                res[i] = -v[i];
            return res;
        }
        public static VectorN operator +(VectorN v1, VectorN v2) {
            VectorN res = new VectorN(v1.Length);
            for (int i = 0; i < res.Length; i++)
                res[i] = v1[i] + v2[i];
            return res;
        }
        public static VectorN operator -(VectorN v1, VectorN v2) {
            VectorN res = new VectorN(v1.Length);
            for (int i = 0; i < res.Length; i++)
                res[i] = v1[i] - v2[i];
            return res;
        }
        public static VectorN operator *(double k, VectorN v) {
            VectorN res = new VectorN(v.Length);
            for (int i = 0; i < res.Length; i++)
                res[i] = v[i] * k;
            return res;
        }
        public static VectorN operator *(VectorN v, double k) {
            return k * v;
        }
        public static VectorN operator /(VectorN v, double k) {
            VectorN res = new VectorN(v.Length);
            for (int i = 0; i < res.Length; i++)
                res[i] = v[i] / k;
            return res;
        }

        public VectorN Clone() {
            VectorN res = new VectorN(v.Length);
            for (int i = 0; i < v.Length; i++)
                res[i] = v[i];
            return res;
        }

		public void Join(VectorN source){
			double[] nv = new double[v.Length + source.Length];
			for (int i = 0; i < v.Length; i++)
				nv [i] = v [i];
			for (int i = 0; i < source.Length; i++)
				nv [i + v.Length] = source [i];
			v = nv;
		}
    }
}
