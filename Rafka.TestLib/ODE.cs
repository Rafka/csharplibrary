﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rafka.TestLib {
    abstract public class ODE {
        public double ts { get; protected set; }
        public double te { get; protected set; }
        public VectorN x0 { get; protected set; }

        abstract public VectorN Feval(double t, VectorN x);
    }
}
