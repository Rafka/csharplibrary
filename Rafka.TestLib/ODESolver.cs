﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rafka.TestLib {
    public static class ODESolver {
        public static double dt { get; set; } = 0.00001;

		public static List<VectorN> rk4(ODE func) {
			List<VectorN> list = new List<VectorN> ();
            VectorN k1, k2, k3, k4;

            VectorN y = func.x0.Clone();
			double t;
			list.Clear ();

			for(t = func.ts; t <= func.te; t+=dt) {
				list.Add (new VectorN (t));
				list [list.Count - 1].Join (y);
                k1 = func.Feval(t, y);
                k2 = func.Feval(t + dt / 2, y + dt * k1 / 2);
                k3 = func.Feval(t + dt / 2, y + dt * k2 / 2);
                k4 = func.Feval(t + dt, y + dt * k3);
                y += (dt / 6) * (k1 + 2 * k2 + 2 * k3 + k4);
            }
			list.Add (new VectorN (t));
			list [list.Count - 1].Join (y);

            return list;
        }
    }
}
