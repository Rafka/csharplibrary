﻿namespace Rafka.MathLib {
    internal enum TokenType { NUM, VAR, OPR, BRC, FNC, CNS}

    internal class Token {
        internal TokenType Ident { get; }
        internal string Buffer { get; }
        internal int Priority { get; }
        internal bool LeftCat { get; }

        internal Token(TokenType type, string buf) {
            Ident = type;
            Buffer = buf;
            Priority = GetPriority(buf);
            LeftCat = IsLeftCat(buf);
        }

        private static int GetPriority(string s) {
            switch (s) {
                case "+": return 2;
                case "-": return 2;
                case "*": return 1;
                case "/": return 1;
                case "%": return 1;
                case "^": return 0;
                case "_": return 1;
                default: return int.MaxValue;
            }
        }

        private bool IsLeftCat(string s) {
            return s != "^" && s != "_";
        }
    }
}
