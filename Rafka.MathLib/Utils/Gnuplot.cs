﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rafka.MathLib.Utils {
    public class Gnuplot : Process {
        public Gnuplot() {
            string path;
            Console.Write("PATH(gnuplot.exe) : ");
            path = Console.ReadLine();
            path.Replace("\\", "\\\\");

            StartInfo.FileName = path;
            StartInfo.UseShellExecute = false;
            StartInfo.RedirectStandardInput = true;
        }
        public Gnuplot(string path) {
            StartInfo.FileName = path;
            StartInfo.UseShellExecute = false;
            StartInfo.RedirectStandardInput = true;
        }

        public void SetXRange(double start, double end) {
            StandardInput.WriteLine("set xrange [" + start + ":" + end + "]");
        }

        public void SetYRange(double start, double end) {
            StandardInput.WriteLine("set yrange [" + start + ":" + end + "]");
        }

        public void SetZRange(double start, double end) {
            StandardInput.WriteLine("set zrange [" + start + ":" + end + "]");
        }
    }
}
