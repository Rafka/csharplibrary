﻿using System.Collections.Generic;

namespace Rafka.MathLib {
    internal static class Converter {
        private static Stack<Token> mem = new Stack<Token>();

        private static bool isNumber(char c) {
            return '0' <= c && c <= '9';
        }

        private static bool isAlphabet(char c) {
            return ('A' <= c && c <= 'Z') || ('a' <= c && c <= 'z');
        }

        private static bool isOperator(char c) {
            switch (c) {
                case '+': case '-': case '*': case '/': case '^': case '%': return true;
                default: return false;
            }
        }

        private static bool isBracket(char c) {
            return c == '(' || c == ')';
        }

        private static bool isFunction(string s) {
            switch (s) {
                case "SIN": case "COS": case "TAN": case "EXP": case "LOG": return true;
                default: return false;
            }
        }

        private static bool isConst(string s) {
            switch (s) {
                case "PI": case "E": case "EPS": return true;
                default: return false;
            }
        }

        internal static Queue<Token> Tokenize(string s) {
            Queue<Token> expr = new Queue<Token>();
            int si;
            string buf;

            expr.Clear();

            for(int i = 0; i < s.Length; i++) {
                if (isNumber(s[i])) {
                    si = i;
                    while (i < s.Length && (isNumber(s[i]) || s[i] == '.')) i++;
                    expr.Enqueue(new Token(TokenType.NUM, s.Substring(si, i - si)));
                    i--;
                } else if (isAlphabet(s[i])) {
                    si = i;
                    while (i < s.Length && (isNumber(s[i]) || isAlphabet(s[i]))) i++;
                    buf = s.Substring(si, i - si);
                    i--;
                    if (isFunction(buf.ToUpper()))
                        expr.Enqueue(new Token(TokenType.FNC, buf));
                    else if (isConst(buf.ToUpper()))
                        expr.Enqueue(new Token(TokenType.CNS, buf));
                    else
                        expr.Enqueue(new Token(TokenType.VAR, buf));
                } else if (isOperator(s[i])) {
                    if ((i == 0 || s[i - 1] == '(') && s[i] == '-')
                        expr.Enqueue(new Token(TokenType.OPR, "_"));
                    else
                        expr.Enqueue(new Token(TokenType.OPR, s.Substring(i, 1)));
                } else if (isBracket(s[i])) {
                    expr.Enqueue(new Token(TokenType.BRC, s.Substring(i, 1)));
                }
            }

            return expr;
        }

        internal static void ToRPN(Queue<Token> expr) {
            Token t;
            int n = expr.Count;

            mem.Clear();

            for(int i = 0; i < n; i++) {
                t = expr.Dequeue();
                switch (t.Ident) {
                    case TokenType.NUM:
                    case TokenType.CNS:
                    case TokenType.VAR:
                        expr.Enqueue(t);
                        break;
                    case TokenType.FNC:
                        mem.Push(t);
                        break;
                    case TokenType.BRC:
                        if (t.Buffer == "(")
                            mem.Push(t);
                        if (t.Buffer == ")") {
                            while (!(mem.Peek().Buffer == "(")) expr.Enqueue(mem.Pop());
                            t = mem.Pop();
                            if (mem.Count != 0 && mem.Peek().Ident == TokenType.FNC)
                                expr.Enqueue(mem.Pop());
                        }
                        break;
                    case TokenType.OPR:
                        if (mem.Count == 0) {
                            mem.Push(t);
                            break;
                        }
                        while(mem.Count != 0 && mem.Peek().Ident == TokenType.OPR) {
                            if ((t.LeftCat && t.Priority >= mem.Peek().Priority) || t.Priority > mem.Peek().Priority)
                                expr.Enqueue(mem.Pop());
                            else
                                break;
                        }
                        mem.Push(t);
                        break;
                }
            }
            while (mem.Count != 0)
                expr.Enqueue(mem.Pop());
        }
    }
}
