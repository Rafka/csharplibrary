﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rafka.MathLib {
    public class Expression {
        private Queue<Token> expr;
        public string RawExpr { get; private set; }
        private Dictionary<string, double> varns;
        
        public Expression(string expression) {
            RawExpr = expression;
            expr = Converter.Tokenize(expression.Replace(" ", ""));
            Converter.ToRPN(expr);
        }

        internal void PrintTokenedExpression() {
            Token t;
            int n = expr.Count;

            for(int i = 0; i < n; i++) {
                t = expr.Dequeue();
                Console.WriteLine(t.Ident + " " + t.Buffer + (t.Ident == TokenType.OPR ? " " + t.Priority : ""));
                expr.Enqueue(t);
            }
        }

        public void PrintExpression() {
            Console.WriteLine(RawExpr);
        }

        public void PrintRPN() {
            int n = expr.Count;
            Token t;
            StringBuilder sb = new StringBuilder();

            sb.Clear();
            for(int i = 0; i < n; i++) {
                t = expr.Dequeue();
                sb.Append(t.Buffer + " ");
                expr.Enqueue(t);
            }

            Console.WriteLine(sb.ToString().Trim());
        }

        public double Eval() {
            Stack<double> ans = new Stack<double>();
            Token t;
            double p;
            int n = expr.Count;

            for(int i = 0; i < n; i++) {
                t = expr.Dequeue();
                switch (t.Ident) {
                    case TokenType.NUM:
                        ans.Push(double.Parse(t.Buffer));
                        break;
                    case TokenType.VAR:
                        if (varns?.ContainsKey(t.Buffer) ?? false)
                            ans.Push(varns[t.Buffer]);
                        else
                            throw new ArgumentException("計算するために必要な変数の値の情報が不足しています。", "vs");
                        break;
                    case TokenType.OPR:
                        p = ans.Pop();
                        switch (t.Buffer) {
                            case "+": ans.Push(ans.Pop() + p); break;
                            case "-": ans.Push(ans.Pop() - p); break;
                            case "*": ans.Push(ans.Pop() * p); break;
                            case "/": ans.Push(ans.Pop() / p); break;
                            case "%": ans.Push(ans.Pop() % p); break;
                            case "^": ans.Push(Math.Pow(ans.Pop(), p)); break;
                            case "_": ans.Push(-p); break;
                        }
                        break;
                    case TokenType.CNS:
                        switch (t.Buffer.ToUpper()) {
                            case "pi": case "PI": ans.Push(Math.PI); break;
                            case "e": case "E": ans.Push(Math.E); break;
                            case "eps": case "EPS": ans.Push(double.Epsilon); break;
                            case "epsf": case "EPSF": ans.Push(float.Epsilon); break;
                        }
                        break;
                    case TokenType.FNC:
                        switch (t.Buffer) {
                            case "sin": case "SIN": ans.Push(Math.Sin(ans.Pop())); break;
                            case "cos": case "COS": ans.Push(Math.Cos(ans.Pop())); break;
                            case "tan": case "TAN": ans.Push(Math.Tan(ans.Pop())); break;
                            case "Sin": ans.Push(Math.Asin(ans.Pop())); break;
                            case "Cos": ans.Push(Math.Acos(ans.Pop())); break;
                            case "Tan": ans.Push(Math.Atan(ans.Pop())); break;
                            case "exp": case "EXP": case "Exp": ans.Push(Math.Exp(ans.Pop())); break;
                            case "log": case "LOG": case "Log": ans.Push(Math.Log(ans.Pop())); break;
                        }
                        break;
                }
                expr.Enqueue(t);
            }

            return ans.Pop();
        }
        public double Eval(Dictionary<string, double> vs) {
            varns = vs;
            return Eval();
        }
    }
}
