﻿using System;
using System.Numerics;
using System.Text;

namespace Rafka.MathLib.Complex {
    public struct Complex {
        public double Re;
        public double Im;

        private bool IsZero(double n) {
            return -1e-30 < n && n < 1e-30;
        }
        private bool Equal(double n1, double n2) {
            return n2 - 1e-30 < n1 && n1 < n2 + 1e-30;
        }

        public Complex(double re, double im) {
            Re = re;
            Im = im;
        }

        public static Complex FromPolar(double magnitude, double phase) {
            return magnitude * new Complex(Math.Cos(phase), Math.Sin(phase));
        }

        public static Complex operator +(Complex c1, Complex c2) {
            return new Complex(c1.Re + c2.Re, c1.Im + c2.Im);
        }

        public static Complex operator -(Complex c) {
            return new Complex(-c.Re, -c.Im);
        }

        public static Complex operator -(Complex c1, Complex c2) {
            return c1 + (-c2);
        }

        public static Complex operator *(Complex c1, Complex c2) {
            return new Complex(c1.Re * c2.Re - c1.Im * c2.Im, c1.Im * c2.Re + c1.Re * c2.Im);
        }

        public static Complex operator /(Complex c1, Complex c2) {
            return c1 * c2.Conjugate / (c2 * c2.Conjugate).Re;
        }

        public static bool operator==(Complex left, Complex right) {
            return left.Equals(right);
        }
        public static bool operator!=(Complex left, Complex right) {
            return !left.Equals(right);
        }

        public double Abs {
            get {
                return Math.Sqrt((this * Conjugate).Re);
            }
        }

        public double Argument {
            get {
                if (Re > 0) return Math.Atan(Im / Re);
                if (IsZero(Re))
                    if (Im > 0) return Math.PI / 2.0;
                    else return 3 * Math.PI / 2.0;
                if (Re < 0) return Math.Atan(Im / Re) + Math.PI;
                return double.NaN;
            }
        }

        public Complex Conjugate {
            get {
                return new Complex(Re, -Im);
            }
        }

        public void Print() {
            Print("###0.#######", "###0.#######");
        }
        public void Print(string format) {
            Print(format, format);
        }
        public void Print(string format_re, string format_im) {
            Console.Write(Re.ToString(format_re));
            if (!IsZero(Im)) {
                Console.Write(Im < 0 ? "-" : "+");
                Console.Write("i" + Math.Abs(Im).ToString(format_im));
            }
        }
        public void PrintReal() {
            PrintReal("###0.#######");
        }
        public void PrintReal(string format) {
            Console.Write(Re.ToString(format));
        }
        public void PrintImag() {
            PrintImag("###0.#######");
        }
        public void PrintImag(string format) {
            Console.Write(Im.ToString(format));
        }

        public override string ToString() {
            return ToString("###0.#######");
        }
        public string ToString(string format) {
            StringBuilder sb = new StringBuilder();
            sb.Clear();

            if (IsZero(Re) && IsZero(Im))
                sb.AppendFormat("{0,12}", 0);
            else
                sb.AppendFormat("{0,12:" + format + "}", Re);
            if (!IsZero(Im)) {
                if (sb.Length == 0 && Im < 0)
                    sb.Append("-");
                else
                    sb.Append(Im < 0 ? "-" : "+");
                sb.AppendFormat("i{0,-12:" + format + "}", Im);
            }

            return sb.ToString();
        }

        public static Complex Zero {
            get {
                return new Complex(0, 0);
            }
        }

        public static Complex RealUnit {
            get {
                return new Complex(1, 0);
            }
        }

        public static Complex ImagUnit {
            get {
                return new Complex(0, 1);
            }
        }

        public static Complex Unit {
            get {
                return new Complex(1, 1);
            }
        }

        public static Complex RealEpsilon {
            get {
                return new Complex(double.Epsilon, 0);
            }
        }
        public static Complex ImagEpsilon {
            get {
                return new Complex(0, double.Epsilon);
            }
        }

        public bool Equals(Complex c) {
            return Equal(Re, c.Re) && Equal(Im, c.Im);
        }

        public static Complex Exp(Complex c) {
            return Math.Exp(c.Re) * new Complex(Math.Cos(c.Im), Math.Sin(c.Im));
        }

        public static Complex Log(Complex c) {
            return new Complex(Math.Log(c.Abs), c.Argument);
        }

        public static Complex LogN(double n, Complex c) {
            return Log(c) / Math.Log(n);
        }

        public static Complex Log10(Complex c) {
            return LogN(10.0, c);
        }

        public static Complex Pow(Complex a, Complex z) {
            return Exp(z * Log(a));
        }

        public static Complex Sqrt(Complex c) {
            return FromPolar(Math.Sqrt(c.Abs), c.Argument / 2.0);
        }

        public static Complex Sin(Complex c) {
            return new Complex(Math.Sin(c.Re) * Math.Cosh(c.Im), Math.Cos(c.Re) * Math.Sinh(c.Im));
        }

        public static Complex Sinh(Complex c) {
            return new Complex(Math.Sinh(c.Re) * Math.Cos(c.Im), Math.Cosh(c.Re) * Math.Sin(c.Im));
        }

        public static Complex Cos(Complex c) {
            return new Complex(Math.Cos(c.Re) * Math.Cosh(c.Im), -(Math.Sin(c.Re) * Math.Sinh(c.Im)));
        }

        public static Complex Cosh(Complex c) {
            return new Complex(Math.Cosh(c.Re) * Math.Cos(c.Im), Math.Sinh(c.Re) * Math.Sin(c.Im));
        }

        public static Complex Tan(Complex c) {
            return Sin(c) / Cos(c);
        }

        public static Complex Tanh(Complex c) {
            return Sinh(c) / Cosh(c);
        }

        public static Complex Asin(Complex c) {
            return -ImagUnit * Log(ImagUnit * c + Sqrt(RealUnit - c * c));
        }

        public static Complex Acos(Complex c) {
            return -ImagUnit * Log(c + ImagUnit * Sqrt(RealUnit - c * c));
        }

        public static Complex Atan(Complex c) {
            return ImagUnit / new Complex(2.0, 0.0) * (Log(RealUnit - ImagUnit * c) - Log(RealUnit + ImagUnit * c));
        }

        public override bool Equals(object obj) {
            return base.Equals(obj);
        }

        public override int GetHashCode() {
            return base.GetHashCode();
        }

        public static explicit operator Complex(BigInteger val) {
            return new Complex((double)val, 0);
        }

        public static explicit operator Complex(decimal val) {
            return new Complex((double)val, 0);
        }

        public static implicit operator Complex(double val) {
            return new Complex(val, 0);
        }

        public static implicit operator Complex(float val) {
            return new Complex(val, 0);
        }

        public static implicit operator Complex(int val) {
            return new Complex(val, 0);
        }

        public static implicit operator Complex(uint val) {
            return new Complex(val, 0);
        }

        public static implicit operator Complex(long val) {
            return new Complex(val, 0);
        }

        public static implicit operator Complex(ulong val) {
            return new Complex(val, 0);
        }

        public static implicit operator Complex(short val) {
            return new Complex(val, 0);
        }

        public static implicit operator Complex(ushort val) {
            return new Complex(val, 0);
        }

        public static implicit operator Complex(byte val) {
            return new Complex(val, 0);
        }

        public static implicit operator Complex(sbyte val) {
            return new Complex(val, 0);
        }
    }
}
