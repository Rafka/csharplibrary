﻿using System;
using System.Text;

namespace Rafka.MathLib.Real {
    public class Matrix {
        private double[] e;
        internal int r { get; private set; }
        internal int c { get; private set; }

        private void CopyArrayToMatrix(double[] elms) {
            e = new double[elms.Length];
            for (int i = 0; i < e.Length; i++)
                e[i] = elms[i];
        }
        private void CopyArrayToMatrix(double[,] elms) {
            e = new double[elms.Length];

            for (int i = 0; i < r; i++)
                for (int j = 0; j < c; i++)
                    e[c * i + j] = elms[i, j];
        }

        public Matrix(int rowLength, int columnLength) {
            r = rowLength;
            c = columnLength;
            e = new double[r * c];
            Clear();
        }
        public Matrix(int rowLength, int columnLength, double[] elements) {
            r = rowLength;
            c = columnLength;
            CopyArrayToMatrix(elements);
        }
        public Matrix(double[,] elements) {
            r = elements.GetLength(0);
            c = elements.GetLength(1);
            CopyArrayToMatrix(elements);
        }

        public int RowLength {
            get { return r; }
        }

        public int ColumnLength {
            get { return c; }
        }

        public int AllLength {
            get { return e.Length; }
        }

        public double this[int i, int j] {
            get {
                if (0 <= i && i < r && 0 <= j && j < c)
                    return e[c * i + j];
                else
                    throw new ArgumentException("領域外を参照しています。");
            }
            set {
                if (0 <= i && i < r && 0 <= j && j < c)
                    e[c * i + j] = value;
                else
                    throw new ArgumentException("領域外を参照しています。");
            }
        }

        public Matrix Clone {
            get { return new Matrix(r, c, e); }
        }

        public static Matrix operator -(Matrix m) {
            Matrix res = new Matrix(m.r, m.c);

            for (int i = 0; i < m.r; i++)
                for (int j = 0; j < m.c; j++)
                    res[i, j] = -m[i, j];

            return res;
        }
        public static Matrix operator +(Matrix m1, Matrix m2) {
            if (m1.r != m2.r || m1.c != m2.c)
                throw new ArgumentException("加算不可：2つの行列の形が異なります。");

            Matrix res = m1.Clone;
            for (int i = 0; i < res.r; i++)
                for (int j = 0; j < res.c; j++)
                    res[i, j] += m2[i, j];

            return res;
        }
        public static Matrix operator -(Matrix m1, Matrix m2) {
            if (m1.r != m2.r || m1.c != m2.c)
                throw new ArgumentException("減算不可：2つの行列の形が異なります。");

            Matrix res = m1.Clone;
            for (int i = 0; i < res.r; i++)
                for (int j = 0; j < res.c; j++)
                    res[i, j] -= m2[i, j];

            return res;
        }
        public static Matrix operator *(double k, Matrix m) {
            Matrix res = m.Clone;
            for (int i = 0; i < res.r; i++)
                for (int j = 0; j < res.c; j++)
                    res[i, j] *= k;

            return res;
        }
        public static Matrix operator *(Matrix m, double k) {
            return k * m;
        }
        public static Matrix operator *(Matrix m1, Matrix m2) {
            if (m1.c != m2.r)
                throw new ArgumentException("積算不可：2つの行列の形が不適切です。");

            Matrix res = new Matrix(m1.r, m2.c);
            double tmp;
            for(int i = 0; i < res.r; i++) {
                for(int j = 0; j < res.c; j++) {
                    tmp = 0.0;
                    for (int k = 0; k < m1.c; k++)
                        tmp += m1[i, k] * m2[k, j];
                    res[i, j] = tmp;
                }
            }

            return res;
        }
        public static Matrix operator /(Matrix m, double k) {
            Matrix res = m.Clone;
            for (int i = 0; i < res.r; i++)
                for (int j = 0; j < res.c; j++)
                    res[i, j] /= k;

            return res;
        }

        public string ToString(string format) {
            StringBuilder sb = new StringBuilder();
            sb.Clear();

            for (int i = 0; i < r; i++) {
                for (int j = 0; j < c; j++) {
                    sb.Append(e[c * i + j].ToString(format));
                    if (j != c - 1) sb.Append(" ");
                    else sb.Append("\n");
                }
            }

            return sb.ToString();
        }
        public override string ToString() {
            return ToString("###0.#######");
        }

        public void Print(string format) {
            Console.WriteLine(ToString(format));
        }
        public void Print() {
            Console.WriteLine(ToString());
        }

        public static Matrix Zero(int rowLength, int columnLength) {
            return new Matrix(rowLength, columnLength);
        }

        public static Matrix Identity(int size) {
            Matrix res = new Matrix(size, size);
            for (int i = 0; i < size; i++)
                res[i, i] = 1.0;
            return res;
        }

        public void Clear() {
            for (int i = 0; i < e.Length; i++)
                e[i] = 0;
        }

        public Matrix Transpose() {
            Matrix res = new Matrix(r, c);

            for (int i = 0; i < r; i++)
                for (int j = 0; j < c; j++)
                    res[i, j] = this[j, i];

            return res;
        }

        public void RowSwap(int i, int j) {
            double tmp;

            for(int k = 0; k < c; k++) {
                tmp = this[i, k];
                this[i, k] = this[j, k];
                this[j, k] = tmp;
            }
        }

        public void ColumnSwap(int i, int j) {
            double tmp;

            for(int k = 0; k < r; k++) {
                tmp = this[k, i];
                this[k, i] = this[k, j];
                this[k, j] = tmp;
            }
        }

        public bool IsSquare() {
            return r == c;
        }
    }
}
