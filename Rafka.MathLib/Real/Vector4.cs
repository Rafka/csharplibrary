﻿namespace Rafka.MathLib.Real {
    public class Vector4 : Vector {
        public Vector4() : base(4) { }
        public Vector4(double x, double y, double z, double w) : base(x, y, z, w) { }

        public double x {
            get { return e[0]; }
            set { e[0] = value; }
        }

        public double y {
            get { return e[1]; }
            set { e[1] = value; }
        }

        public double z {
            get { return e[2]; }
            set { e[2] = value; }
        }

        public double w {
            get { return e[3]; }
            set { e[3] = value; }
        }
    }
}
