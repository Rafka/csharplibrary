﻿namespace Rafka.MathLib.Real {
    public class Vector3 : Vector {
        public Vector3() : base(3) { }
        public Vector3(double x, double y, double z) : base(x, y, z) { }

        public double x {
            get { return e[0]; }
            set { e[0] = value; }
        }

        public double y {
            get { return e[1]; }
            set { e[1] = value; }
        }

        public double z {
            get { return e[2]; }
            set { e[2] = value; }
        }

        public static Vector3 Cross(Vector3 v1, Vector3 v2) {
            return new Vector3(v1.y * v2.z - v1.z * v2.y, v1.z * v2.x - v1.x * v2.z, v1.x * v2.y - v1.y * v2.x);
        }
    }
}
