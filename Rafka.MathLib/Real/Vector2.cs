﻿namespace Rafka.MathLib.Real {
    public class Vector2 : Vector {
        public Vector2() : base(2) { }
        public Vector2(double x, double y) : base(x, y) { }

        public double x {
            get { return e[0]; }
            set { e[0] = value; }
        }

        public double y {
            get { return e[1]; }
            set { e[1] = value; }
        }
    }
}
