﻿using System;
using System.Text;

namespace Rafka.MathLib.Real {
    public class Vector {
        protected double[] e;

        public Vector(int length) {
            e = new double[length];
            for (int i = 0; i < length; i++)
                e[i] = 0.0;
        }
        public Vector(params double[] elements) {
            e = new double[elements.Length];
            for (int i = 0; i < elements.Length; i++)
                e[i] = elements[i];
        }

        public int Length {
            get { return e.Length; }
        }

        public double this[int i] {
            get {
                if (0 <= i && i < e.Length)
                    return e[i];
                else
                    throw new ArgumentException("領域外を参照しています。");
            }
            set {
                if (0 <= i && i < e.Length)
                    e[i] = value;
                else
                    throw new ArgumentException("領域外を参照しています。");
            }
        }

        public Vector Clone {
            get { return new Vector(e); }
        }

        public static Vector operator -(Vector v) {
            Vector res = new Vector(v.Length);

            for (int i = 0; i < res.Length; i++)
                res[i] = -v[i];

            return res;
        }
        public static Vector operator +(Vector v1, Vector v2) {
            if (v1.Length != v2.Length)
                throw new ArgumentException("加算不可：2つのベクトルの長さが異なります。");

            Vector res = v1.Clone;

            for (int i = 0; i < res.Length; i++)
                res[i] += v2[i];

            return res;
        }
        public static Vector operator -(Vector v1, Vector v2) {
            if (v1.Length != v2.Length)
                throw new ArgumentException("減算不可：2つのベクトルの長さが異なります。");

            Vector res = v1.Clone;

            for (int i = 0; i < res.Length; i++)
                res[i] -= v2[i];

            return res;
        }
        public static Vector operator *(double k, Vector v) {
            Vector res = v.Clone;

            for (int i = 0; i < res.Length; i++)
                res[i] *= k;

            return res;
        }
        public static Vector operator *(Vector v, double k) {
            return k * v;
        }
        public static Vector operator /(Vector v, double k) {
            Vector res = v.Clone;
            for (int i = 0; i < v.Length; i++)
                res[i] /= k;

            return res;
        }

        public string ToString(string format) {
            StringBuilder sb = new StringBuilder();
            sb.Clear();

            for (int i = 0; i < e.Length; i++)
                sb.Append(e[i].ToString(format) + "\n");

            return sb.ToString();
        }
        public override string ToString() {
            return ToString("###0.########");
        }

        public void Print(string format) {
            Console.WriteLine(ToString(format));
        }
        public void Print() {
            Console.WriteLine(ToString());
        }

        public void Join(Vector source) {
            double[] ne = new double[e.Length + source.Length];

            for (int i = 0; i < e.Length; i++)
                ne[i] = e[i];
            for (int i = 0; i < source.Length; i++)
                ne[e.Length + i] = source[i];
            e = ne;
        }

        public static Vector Zero(int length) {
            return new Vector(length);
        }

        public static Vector One(int length) {
            Vector res = new Vector(length);

            for (int i = 0; i < length; i++)
                res[i] = 1.0;

            return res;
        }

        public double Norm(int n) {
            double sum = 0.0, tmp;

            for (int i = 0; i < e.Length; i++) {
                tmp = 1.0;
                for (int j = 0; j < n; j++) tmp *= e[i];
                sum += tmp;
            }

            return Math.Pow(sum, 1.0 / n);
        }

        public double Size {
            get {
                double sum = 0.0;

                for (int i = 0; i < e.Length; i++)
                    sum += e[i] * e[i];

                return Math.Sqrt(sum);
            }
        }

        public static double Dot(Vector v1, Vector v2) {
            if (v1.Length != v2.Length)
                throw new ArgumentException("2つのベクトルの長さが異なります。");

            double res = 0.0;

            for (int i = 0; i < v1.Length; i++)
                res += v1[i] * v2[i];

            return res;
        }
        public double Dot(Vector v) {
            return Dot(this, v);
        }

        public void Swap(int i, int j) {
            double tmp;
            tmp = e[i];
            e[i] = e[j];
            e[j] = tmp;
        }
    }
}
