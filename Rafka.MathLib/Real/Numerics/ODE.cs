﻿namespace Rafka.MathLib.Real.Numerics {
    abstract public class ODE {
        public double ts { get; protected set; }
        public double te { get; protected set; }
        public Vector x0 { get; protected set; }

        abstract public Vector Feval(double t, Vector x);
    }
}