﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Rafka.MathLib.Real.Numerics {
    public class LUSolver {
        private Matrix LU;
        private Queue<Point> SwapMem;

        private bool IsZero(double n) {
            return -double.Epsilon * 1e10 < n && n < double.Epsilon * 1e10;
        }

        public LUSolver(Matrix A) {
            if (A.IsSquare()) {
                MakeLUMatrix(A);
            } else {
                throw new ArgumentException("与えられる係数行列は正方行列である必要があります。");
            }
        }

        private void MakeLUMatrix(Matrix A) {
            LU = new Matrix(A.r, A.c);

            for (int i = 1; i <= A.r; i++)
                for (int j = 1; j <= A.c; j++)
                    LU[i, j] = A[i, j];

            int max_pos;
            SwapMem = new Queue<Point>();
            SwapMem.Clear();
            for(int i = 0; i < LU.r; i++) {
                if (IsZero(LU[i, i])) {
                    max_pos = i;
                    for (int j = i + 1; j < LU.r; j++)
                        if (LU[max_pos, i] < LU[j, i]) max_pos = j;
                    LU.RowSwap(i, max_pos);
                    SwapMem.Enqueue(new Point(i, max_pos));
                }
            }
            Decomp();
        }

        private void Decomp() {
            double p;
            for(int k = 0; k < LU.r; k++) {
                for(int i = k + 1; i < LU.r; i++) {
                    p = LU[i, k] / LU[k, k];
                    for (int j = k + 1; j < LU.r; j++)
                        LU[i, j] -= p * LU[k, j];
                    LU[i, k] = p;
                }
            }
        }

        private void SolveSetUp(Vector v, Vector b) {
            int c = SwapMem.Count;
            Point mem;

            for (int i = 1; i <= v.Length; i++) b[i] = v[i];
            for(int i = 0; i < c; i++) {
                mem = SwapMem.Dequeue();
                b.Swap(mem.X, mem.Y);
                SwapMem.Enqueue(mem);
            }
        }

        public Vector Solve(Vector v) {
            Vector result = new Vector(v.Length);
            double sum;

            SolveSetUp(v, result);

            for(int i = 1; i < result.Length; i++) {
                sum = 0.0;
                for (int j = 0; j <= i - 1; j++)
                    sum += LU[i, j] * result[j];
                result[i] -= sum;
            }

            for(int i = result.Length - 1; i >= 0; i--) {
                sum = 0.0;
                for (int j = i + 1; j <= result.Length; j++)
                    sum += LU[i, j] * result[j];
                result[i] = (result[i] - sum) / LU[i, i];
            }

            return result;
        }

        public void PrintLUMatrix() {
            LU.Print();
        }
    }
}
