﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rafka.MathLib.Real.Numerics {
    public static class ODESolver {
        public static double dt { get; set; } = 0.001;

        public static List<Vector> rk4(ODE func) {
            List<Vector> list = new List<Vector>();
            Vector k1, k2, k3, k4;

            Vector y = func.x0.Clone;
            double t;
            list.Clear();

            for (t = func.ts; t <= func.te; t += dt) {
                list.Add(new Vector(t));
                list[list.Count - 1].Join(y);
                k1 = func.Feval(t, y);
                k2 = func.Feval(t + dt / 2, y + dt * k1 / 2);
                k3 = func.Feval(t + dt / 2, y + dt * k2 / 2);
                k4 = func.Feval(t + dt, y + dt * k3);
                y += (dt / 6) * (k1 + 2 * k2 + 2 * k3 + k4);
            }
            list.Add(new Vector(t));
            list[list.Count - 1].Join(y);

            return list;
        }

        public static void rk4Step(ODE func, double t, Vector val) {
            Vector k1, k2, k3, k4;

            k1 = func.Feval(t, val);
            k2 = func.Feval(t + dt / 2, val + dt * k1 / 2);
            k3 = func.Feval(t + dt / 2, val + dt * k2 / 2);
            k4 = func.Feval(t + dt, val + dt * k3);

            val += (dt / 6) * (k1 + 2 * k2 + 2 * k3 + k4);
        }
    }
}
